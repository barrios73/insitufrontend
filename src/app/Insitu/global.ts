'use strict';

export let DashStartTime: string = "20"; 

//#region Summary Page
export let series: any[] = [
  {
    name: "Predicted Yield",
    data: [],
    markers:{
      size:5,
      type: 'circle'
    }
  },
  {
    name: "Target",
    data: [],
    markers:{
      visible: false
    }
  }
];
export let nextCleanUpTime: Date; 
export function updateNextCleanUpTime(value: Date){
  nextCleanUpTime = value;
}
export function resetSeries(){
  series[0].data = [];
  series[1].data = [];
}
//#endregion

//#region Machine Detail page
export let MachineID = "";
export function UpdateMachineID(value: string){
  MachineID = value;
}
export let machineSeries: any[] = [
  {
    name: "Predicted Yield",
    data: [],
    markers:{
      size:5,
      type: 'circle'
    }
  },
  {
    name: "Target",
    data: [],
    markers:{
      visible:false
    }
  }
];
export let nextMachineCleanUpTime: Date; 
export function updateNextMachineCleanUpTime(value: Date){
  nextMachineCleanUpTime = value;
}
export function resetMachineSeries(){
  machineSeries[0].data = [];
  machineSeries[1].data = [];
}
//#endregion