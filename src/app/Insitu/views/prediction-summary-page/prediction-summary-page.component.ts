import { Component, OnInit } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { HttpClient } from '@angular/common/http';
import { PassRate, PassRateWithProcessId } from '../../passRate';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { SortDescriptor } from '@progress/kendo-data-query';
import * as global from '../../global';
import { formatDate } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { ENDPOINT } from '../../settings/environment';

@Component({
  selector: 'app-prediction-summary-page',
  templateUrl: './prediction-summary-page.component.html',
  styleUrls: ['./prediction-summary-page.component.scss']
})
export class PredictionSummaryPageComponent implements OnInit {

  public predictionResultList: any[];
  public ResultSummaryGridData: any[];
  public passRateList: Array<PassRate> = [];
  public passRateWithProcessIdList: Array<PassRateWithProcessId> = [];
  private _hubConnection: HubConnection;
  public dashboardtime: string;
  public time: Date = new Date();
  public pageTitle: string = '';
  public pageSubtitle: string;
  timer;
  chartTimer;
  public widthValue: number = 30;
  public PredictedPassResult: string = "000.00";
  public PredictedFailResult: string = "0.00";
  public TotalFailCase: number = 0;
  public TotalCase: number = 0;
  public TotalFailOverTotalCase: string = "";
  public listStatus: Array<string> = ['All', 'Pass', 'Fail'];
  public YieldByMachineGridData: any[];
  public YieldStatusByMachine: string = "All";
  public YieldByProcessGridData: any[];
  public YieldStatusByProcess: string = "All";
  public HardedCodeTargetRate: number = 50;
  public realTimeChartSeries: any[];
  //Harded-code the daily chart
  public dailyChartSeries: any[] = [
    {
      name: "Predicted Yield",
      data: [69.5, 86.7, 74.3, 80.05, 82.6, 92, 61.3, 74, 66.5, 78.3, 80.5, 88.5, 73.2, 81],
      markers:{
        size:5,
        type: 'circle'
      }
    },
    {
      name: "Target",
      data: [50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50],
      markers:{
        visible: false
      }
    }
  ];
  public categories: string[] = [];
  
  public sortByMachine: SortDescriptor[] = [
    {
        field: 'machineId',
        dir: 'asc'
    }
  ];
  
  public sortByProcess: SortDescriptor[] = [
    {
        field: 'processId',
        dir: 'asc'
    }
  ];

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { 

    const headers= new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')

    //set the next clean up time if no clean up time is specificed
    if(global.nextCleanUpTime === undefined){
      global.updateNextCleanUpTime(this.calculateNextCleanUpTime());
    }
    else{
      //if current time is more then clean up time. 
      //update the next clean up time and reset the chart
      if(new Date() >= global.nextCleanUpTime){
        global.updateNextCleanUpTime(this.calculateNextCleanUpTime());
        global.resetSeries();
      }
    }

    //loop to declare last 13 days' date
    for(let i=13; i >= 0; i--){
      var currentDate = new Date();
      currentDate.setDate(currentDate.getDate() - i);
      this.categories.push(currentDate.toLocaleDateString('en-GB', {year: "numeric", month: "short", day: "numeric"}));
    }

    //192.168.137.1:5004
    this.http.get(ENDPOINT.API + "/Dashboard/Summary/GetCurrentPredictionResult").subscribe((prediction:any) => {
      
      this.predictionResultList = prediction.data;
      this.calculatePassRate(prediction.data);
      this.ResultSummaryGridData = this.passRateList;
      this.populateYieldByMachineGridData(this.passRateList);
      this.populateYieldByProcessGridData(this.passRateWithProcessIdList);

      //if global series do not have data, populate new data
      if(global.series[0].data.length === 0){
        this.populateRealTimeChart();
      }
      else{
        //direct populate global series into chart
        this.realTimeChartSeries = global.series;
      }
      //populate daily chart
      this.populateDailyChart();

    }, err =>{
      console.log('Something went wrong ', err);
    })   
  }

  ngOnInit(): void {
    
    //let predictionResultList = [];

    //url = Server Url
    this._hubConnection = new HubConnectionBuilder().withUrl(ENDPOINT.SIGNALR).build();
    this._hubConnection.start()
      .then(() => console.log('connection start'))
      .catch(err => {
        console.log('Error while establishing the connection')
      });

      this._hubConnection.on('BroadcastPredictionResult', (prediction) => {
        this.passRateList = [];
        this.passRateWithProcessIdList = [];

        //let predictionResultList = [];
        this.predictionResultList.push(prediction);
        //this.calculatePassRate(prediction);
        this.calculatePassRate(this.predictionResultList);
        this.ResultSummaryGridData = this.passRateList;
        this.populateYieldByMachineGridData(this.passRateList);
        this.populateYieldByProcessGridData(this.passRateWithProcessIdList);
         
      })

      this.timer = setInterval(() => {
        this.dashboardtime = this.createPageSubtitle();
        }, 1000);
        
      this.chartTimer = setInterval(() => {
        //if current time is more then clean up time. 
        //update the next clean up time and reset the chart
        if(new Date() >= global.nextCleanUpTime){
          global.updateNextCleanUpTime(this.calculateNextCleanUpTime());
          global.resetSeries();
        }
        if(global.series[0].data.length === 0 || this.PredictedPassResult !== global.series[0].data[global.series[0].data.length-1])
        {
          //if global series do not have data, or latest pass rate different with last pass rate in global series, re-populate data
          this.populateRealTimeChart();
        }
        //populate daily chart
        this.populateDailyChart();
      }, 15000);  //30sec
      

      this.pageTitle = this.createPageTitle();
  }

  
  ngOnDestroy(){
    clearInterval(this.timer);
    clearInterval(this.chartTimer);
    this._hubConnection.stop()
      .then(() => console.log('Results page connection stop'))
      .catch(err => {
        console.log('Error while stopping the connection')
      });
  }

  public calculateNextCleanUpTime(): Date{
    
    //this method called only when that is no clean up time is specificed or current time is more then clean up time
    let currentDate = new Date();
    //below to cater the scenaria that user launch the application same date with dashboard start time
    //Example 1, Dashboard start time is 8am daily, user launch the application after 8am, the next clean up time will be next day 8am.
    //Example 2, Dashboard start time is 8am daily, user laucnh the application before 8am(anytime from 1am-8am), then the next clean up time will be same date 8am. 
    if(currentDate.getHours() >= Number(global.DashStartTime)){
      currentDate.setDate(currentDate.getDate() + 1);
    }
    
    return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + " " + global.DashStartTime + ":00:00");
  }

  public populateRealTimeChart(){
    const myClonedArray = [];
    global.series.forEach(val => myClonedArray.push(Object.assign({}, val)));
    myClonedArray[0].data.push(this.PredictedPassResult);
    myClonedArray[1].data.push(this.HardedCodeTargetRate);
    this.realTimeChartSeries = myClonedArray;
  }

  public populateDailyChart(){
    
    this.dailyChartSeries[0].data[13] = this.PredictedPassResult;
    const myClonedArray = [];
    this.dailyChartSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));
    this.dailyChartSeries = myClonedArray;
  }

  public calculatePassRate(prediction: any){
    const DistinctMachine = prediction.map(item => item.machineId).filter((value, index, self) => self.indexOf(value) === index);
    const DistinctProcessID = prediction.map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
    
    if(prediction.length > 0 ){
      this.TotalCase = prediction.filter(p => p.predictResult.toLowerCase() === 'fail' || p.predictResult.toLowerCase() === 'pass').length;
      this.TotalFailCase = prediction.filter(p => p.predictResult.toLowerCase() === 'fail').length;      
      this.TotalFailOverTotalCase = this.TotalFailCase.toString() + '/' + this.TotalCase.toString();
      this.PredictedPassResult = ((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100).toFixed(2).toString();
      this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
    }
    DistinctMachine.forEach( (element) => {
      let pr = new PassRate();
      const TotalResult = prediction.filter(p => p.machineId === element && (p.predictResult.toLowerCase() === 'fail' || p.predictResult.toLowerCase() === 'pass'));
      const TotalPassResult = prediction.filter(p => p.predictResult.toLowerCase() === 'pass' && p.machineId === element);
      const rate = TotalPassResult.length / TotalResult.length * 100;
      pr.machineId = element;
      pr.passRate = rate.toFixed(2).toString() + "%";
      pr.targetPassRate = this.HardedCodeTargetRate + "%";
      this.passRateList.push(pr);
    });
  
    DistinctProcessID.forEach( (element) => {
      let pr = new PassRateWithProcessId();
      const TotalResult = prediction.filter(p => p.processId === element);
      const TotalPassResult = prediction.filter(p => p.predictResult.toLowerCase() === 'pass' && p.processId === element);
      const rate = TotalPassResult.length / TotalResult.length * 100;
      pr.processId = element;
      pr.passRate = rate.toFixed(2).toString() + "%";
      pr.targetPassRate = this.HardedCodeTargetRate + "%";
      this.passRateWithProcessIdList.push(pr);
    });
  }
  
  public backgroundColorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;
    let numPassRate: number = Number(passRate.split('%')[0]);
    let numTargetPassRate: number = Number(targetPassRate.split('%')[0]);


    if (numTargetPassRate - numPassRate > 0){
      result = '#ff0000';//red
    }
    else{
      result = '#0f0';//green
    }
    return this.sanitizer.bypassSecurityTrustStyle(result);
  }
  
  public colorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;
    let numPassRate: number = Number(passRate.split('%')[0]);
    let numTargetPassRate: number = Number(targetPassRate.split('%')[0]);


    if (numTargetPassRate - numPassRate > 0){
      result = '#fff';//White
    }
    else{
      result = '#000000';//Black 
    }
    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  public byMachineValueChange(value: any): void {
    this.YieldStatusByMachine = value;
    this.populateYieldByMachineGridData(this.passRateList);
  }

  public byProcessValueChange(value: any): void {
    this.YieldStatusByProcess = value;
    this.populateYieldByProcessGridData(this.passRateWithProcessIdList);
  }

  public populateYieldByMachineGridData(prediction: any){
    switch(this.YieldStatusByMachine.toLowerCase()){
      case "all":{
        this.YieldByMachineGridData = prediction;
        break;
      }
      case "pass":{
        this.YieldByMachineGridData = prediction.filter(p => +p.passRate.split('%')[0] >= +p.targetPassRate.split('%')[0] );
        break;
      }
      case "fail":{
        this.YieldByMachineGridData = prediction.filter(p => +p.passRate.split('%')[0] < +p.targetPassRate.split('%')[0] );
        break;
      }
    }
  }

  public populateYieldByProcessGridData(prediction: any){
    switch(this.YieldStatusByProcess.toLowerCase()){
      case "all":{
        this.YieldByProcessGridData = prediction;
        break;
      }
      case "pass":{
        this.YieldByProcessGridData = prediction.filter(p => +p.passRate.split('%')[0] >= +p.targetPassRate.split('%')[0] );
        break;
      }
      case "fail":{
        this.YieldByProcessGridData = prediction.filter(p => +p.passRate.split('%')[0] < +p.targetPassRate.split('%')[0] );
        break;
      }
    }
  }
  
  public cellColorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;

    if (+passRate.split('%')[0] < +targetPassRate.split('%')[0]){
      result = '#f31700';
    }
    else{
      result = 'transparent';
    }

    return this.sanitizer.bypassSecurityTrustStyle(result);
  }


  public createPageTitle(): string{
    let title = 'Factory Quality Dashboard';
    return title;
  }

  public createPageSubtitle():string{
    return this.calcDashboardTime();
  }

  private calcDashboardTime(): string{
    let d = new Date();
    d.setHours(8);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    let startdatetime = d.getTime();

    let curTime = new Date().getTime();
    

    if (curTime < startdatetime){
      let startdate = new Date((startdatetime - (24*60*60*1000)));
      let curdate = new Date(curTime);

      return startdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});
    }
    else{
      let startdate = new Date(startdatetime);
      let curdate = new Date(curTime);
      return startdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});
    }
  }

}
