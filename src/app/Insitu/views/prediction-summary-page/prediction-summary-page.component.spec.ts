import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictionSummaryPageComponent } from './prediction-summary-page.component';

describe('PredictionSummaryPageComponent', () => {
  let component: PredictionSummaryPageComponent;
  let fixture: ComponentFixture<PredictionSummaryPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PredictionSummaryPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictionSummaryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
