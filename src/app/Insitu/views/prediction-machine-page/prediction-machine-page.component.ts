import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { PassRateWithProcessId } from '../../passRate';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { SortDescriptor } from '@progress/kendo-data-query';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import * as global from '../../global';
import { formatDate } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { ENDPOINT } from '../../settings/environment';


@Component({
  selector: 'app-prediction-machine-page',
  templateUrl: './prediction-machine-page.component.html',
  styleUrls: ['./prediction-machine-page.component.scss']
})
export class PredictionMachinePageComponent implements OnInit {

  public machineID: string;
  public time: Date = new Date();
  public dashboardtime: string;
  timer;
  chartTimer;
  public pageTitle: string = '';
  public predictionResultList: any[];
  public PredictedPassResult: string = "000.00";
  public PredictedFailResult: string = "0.00";
  public TotalFailCase: number = 0;
  public TotalCase: number = 0;
  public TotalFailOverTotalCase: string = "";
  public passRateWithProcessIdList: Array<PassRateWithProcessId> = [];
  private _hubConnection: HubConnection;
  public YieldByProcessGridData: any[];
  public HardedCodeTargetRate: number = 50;
  public realTimeChartSeries: any[];
  public dailyChartSeries: any[] = [
    {
      name: "Predicted Yield",
      data: [69.5, 86.7, 74.3, 80.05, 82.6, 92, 61.3, 74, 66.5, 78.3, 80.5, 88.5, 73.2, 81.3],
      markers:{
        size:5,
        type:'circle'
      }
    },
    {
      name: "Target",
      data: [50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50],
      markers:{
        visible:false
      }
    }
  ];
  public categories: string[] = [];

  public sortByProcess: SortDescriptor[] = [
    {
        field: 'processId',
        dir: 'asc'
    }
  ];

  //for the lot list window
  public showLotList: boolean = false;
  public lotListTitle: string;
  public lotListData: any[];
  public windowTop = 100;
  public windowLeft = 365;



  constructor(private http: HttpClient, private route: ActivatedRoute, private sanitizer: DomSanitizer) { 

    const headers= new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')

    if(global.nextMachineCleanUpTime === undefined){
      global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
    }
    else{
      if(new Date() >= global.nextMachineCleanUpTime){
        global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
        global.resetMachineSeries();
      }
    }

    for(let i=13; i >= 0; i--){
      var currentDate = new Date();
      currentDate.setDate(currentDate.getDate() - i);
      this.categories.push(currentDate.toLocaleDateString('en-GB', {year: "numeric", month: "short", day: "numeric"}));
    }
    
    //"http://192.168.137.1:5004/api
    this.http.get<any>(ENDPOINT.API + "/Dashboard/Summary/GetCurrentPredictionResult").subscribe(prediction => {
      this.predictionResultList = prediction.data;
      this.CalculatePassRate(prediction.data);
      this.YieldByProcessGridData = this.passRateWithProcessIdList;

      if(global.machineSeries[0].data.length === 0){
        this.populateRealTimeChart();
      }
      else{
        this.realTimeChartSeries = global.machineSeries;
      }
      this.populateDailyChart();

    }, err =>{
      console.log('Something went wrong ', err);
    })     
  }

  ngOnInit(): void {
    this.route
    .queryParams
    .subscribe(params => {
      this.machineID = params['machineId'];
    });

    if(global.MachineID === ""){
      global.UpdateMachineID(this.machineID);
    }
    else{
      if(global.MachineID !== this.machineID){
        global.resetMachineSeries();
        global.UpdateMachineID(this.machineID);
      }
    }

    //url = Server Url
    this._hubConnection = new HubConnectionBuilder().withUrl(ENDPOINT.SIGNALR).build();
    this._hubConnection.start()
      .then(() => console.log('result page connection start'))
      .catch(err => {
        console.log('Error while establishing the connection')
      });

    this._hubConnection.on('BroadcastPredictionResult', (prediction) => {
      this.passRateWithProcessIdList = [];
      this.predictionResultList.push(prediction);
      this.CalculatePassRate(this.predictionResultList);
      this.YieldByProcessGridData = this.passRateWithProcessIdList;
    })
    
    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
    }, 1000);
    
    this.chartTimer = setInterval(() => {
      if(new Date() >= global.nextMachineCleanUpTime){
        global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
        global.resetMachineSeries();
      }
      if(global.machineSeries[0].data.length === 0 || this.PredictedPassResult !== global.machineSeries[0].data[global.machineSeries[0].data.length-1])
      {
        this.populateRealTimeChart();
      }
      this.populateDailyChart();
    }, 15000);//15sec
  

    this.pageTitle = this.createPageTitle();

  }


  ngOnDestroy(){
    clearInterval(this.timer);
    clearInterval(this.chartTimer);
    this._hubConnection.stop()
    .then(() => console.log('Results page connection stop'))
    .catch(err => {
      console.log('Error while stopping the connection')
    });
  }

  public calculateNextCleanUpTime(): Date{
    
    let currentDate = new Date();
    if(currentDate.getHours() >= Number(global.DashStartTime)){
      currentDate.setDate(currentDate.getDate() + 1);
    }
    
    return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + " " + global.DashStartTime + ":00:00");
  }

  public populateRealTimeChart(){
    const myClonedArray = [];
    global.machineSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));
    myClonedArray[0].data.push(this.PredictedPassResult);
    myClonedArray[1].data.push(this.HardedCodeTargetRate);
    this.realTimeChartSeries = myClonedArray;
  }

  public populateDailyChart(){
    
    this.dailyChartSeries[0].data[13] = this.PredictedPassResult;
    const myClonedArray = [];
    this.dailyChartSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));
    this.dailyChartSeries = myClonedArray;
  }

  public CalculatePassRate(prediction: any){

    if(prediction.length > 0){
      this.TotalCase = prediction.filter(p => p.machineId === this.machineID && (p.predictResult.toLowerCase() === 'fail' || p.predictResult.toLowerCase() === 'pass')).length;
      this.TotalFailCase = prediction.filter(p => p.predictResult.toLowerCase() === 'fail' && p.machineId === this.machineID).length;
      this.TotalFailOverTotalCase = this.TotalFailCase.toString() + '/' + this.TotalCase.toString();
      this.PredictedPassResult = ((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100).toFixed(2).toString();
      this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
    }
    
    const DistinctProcess = prediction.filter(p => p.machineId === this.machineID).map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
    DistinctProcess.forEach( (process) => {
      let pr = new PassRateWithProcessId();
      const TotalResult = prediction.filter(p => p.machineId === this.machineID && p.processId === process && (p.predictResult.toLowerCase() === 'fail' || p.predictResult.toLowerCase() === 'pass'));
      const TotalPassResult = prediction.filter(p => p.predictResult.toLowerCase() === 'pass' && p.machineId === this.machineID && p.processId === process);
      const rate = TotalPassResult.length / TotalResult.length * 100;
      pr.machineId = this.machineID;
      pr.processId = process;
      pr.passRate = rate.toFixed(2).toString() + "%";
      pr.targetPassRate = this.HardedCodeTargetRate + "%"
      this.passRateWithProcessIdList.push(pr);
    });
  }
  
  public cellColorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;

    if (+passRate.split('%')[0] < +targetPassRate.split('%')[0]){
      result = '#f31700';
    }
    else{
      result = 'transparent';
    }

    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  public createPageTitle(): string{
    let title = 'Machine ' + this.machineID + ': Quality Dashboard';
    return title;
  }


  public createPageSubtitle():string{
    return this.calcDashboardTime();
  }


  private calcDashboardTime(): string{
    let d = new Date();
    d.setHours(8);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    let startdatetime = d.getTime();


    let curTime = new Date().getTime();

    if (curTime < startdatetime){
      let startdate = new Date((startdatetime - (24*60*60*1000)));
      let curdate = new Date(curTime);

      return startdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});
    }
    else{
      let startdate = new Date(startdatetime);
      let curdate = new Date(curTime);
      return startdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});
    }
  }


  public openLotList(status){

    this.http.get(ENDPOINT.API + "/Dashboard/Summary/GetCurrentPredictionResult").subscribe((prediction:any) => {
      this.lotListData = prediction.data.filter(p => p.machineId === this.machineID && p.predictResult.toLowerCase() === status.toLowerCase());
      this.lotListTitle = "Machine: " + this.machineID + " Lot List for " + status.toUpperCase() + " Predictions";
      this.showLotList = true;
    }, err =>{
      console.log('Something went wrong ', err);
    })   
  }

  public closeLotListWindow(){
    this.showLotList = false;
  }



}
