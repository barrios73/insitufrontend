import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictionHistoryPageComponent } from './prediction-history-page.component';

describe('PredictionHistoryPageComponent', () => {
  let component: PredictionHistoryPageComponent;
  let fixture: ComponentFixture<PredictionHistoryPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PredictionHistoryPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictionHistoryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
