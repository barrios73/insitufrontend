import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IntlService } from '@progress/kendo-angular-intl';
import { PreventableEvent, DateTimePickerComponent } from '@progress/kendo-angular-dateinputs';
import { DatePipe } from '@angular/common';
import { SortDescriptor } from '@progress/kendo-data-query';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-prediction-history-page',
  templateUrl: './prediction-history-page.component.html',
  styleUrls: ['./prediction-history-page.component.scss']
})
export class PredictionHistoryPageComponent implements OnInit {

  @Input() public title: string;
  @Input() public events: string[];

  public ResultGridData: any[];

  public startDateValue: Date = new Date();
  public endDateValue: Date = new Date();
  
  public sortByCol: SortDescriptor[] = [
    {
        field: 'createdDateTime',
        dir: 'desc'
    }
  ];

  public ngAfterViewInit(): void {
      // Prevents the popup from closing when blurred, on selecting a value, on cancel, and so on.
      // With the default prevention of the close trigger, you can use only the `toggle` method to close the component.
      //this.dateTimePicker.close.subscribe((event) => event.preventDefault());
  }

  constructor(private http: HttpClient, private intl: IntlService, private sanitizer: DomSanitizer) { 
    this.startDateValue.setHours(0,0,0,0);
    this.endDateValue.setHours(23,59,59,0);
  }

  ngOnInit(): void {
  }

  public search(): void {
    if(this.startDateValue === null){
      alert("Please select start date");
      return ;
    }
    if(this.endDateValue === null){
      alert("Please select end date");
      return ;
    }
    const datepipe: DatePipe = new DatePipe('en-US')
    let formattedStartDate = datepipe.transform(this.startDateValue, 'MM-dd-YYYY HH:mm:ss')
    let formattedEndDate = datepipe.transform(this.endDateValue, 'MM-dd-YYYY HH:mm:ss')

    this.http.get<any>("http://192.168.137.1/DashboardSvc/api/Dashboard/GetHistoryPredictionResult?start=" + formattedStartDate + "&end=" + formattedEndDate + "").subscribe(prediction => {
      this.ResultGridData = prediction;

    }, err =>{
      console.log('Something went wrong ', err);
    })    
    //this.startDateTimePicker.toggle(shouldOpen);
    //this.startDateTimePicker.focus();
  }
  
  public colorCode(code: string): SafeStyle {
    let result;

    switch (code.toLowerCase()) {
     case 'fail' :
       result = '#f31700';
       break;
     default:
       result = 'transparent';
       break;
    }
    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

}
