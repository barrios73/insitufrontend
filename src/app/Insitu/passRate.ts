export class PassRate
{
  machineId: string;
  passRate: string;
  targetPassRate: string;
}
export class PassRateWithProcessId
{
  machineId: string;
  processId: string;
  passRate: string;
  targetPassRate: string;
}